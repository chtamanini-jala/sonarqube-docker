Firts I had to pre set up some commands for sonarqube.

```
sysctl -w vm.max_map_count=262144
sysctl -w fs.file-max=65536
ulimit -n 65536
ulimit -u 4096
```

Then I created a docker-compose.yml into a dir, which contains this info:

```
version: "3.3"
services:
  db:
    image: postgres:12-alpine
    environment:
      - POSTGRES_USER=sonar
      - POSTGRES_PASSWORD=sonar
      - POSTGRES_DB=sonar
    volumes:
      - postgres_data:/var/lib/postgresql/data
    networks:
      - sonarqube-net

  sonarqube:
    image: sonarqube:community
    depends_on:
      - db
    environment:
      - sonar.jdbc.username=sonar
      - sonar.jdbc.url=jdbc:postgresql://db/sonar
      - sonar.jdbc.password=sonar
    ports:
      - 9000:9000
    volumes:
      - sonar_conf:/opt/sonarqube/conf
      - sonar_data:/opt/sonarqube/data
      - sonar_extensions:/opt/sonarqube/extensions
      - sonar_plugins:/opt/sonarqube/lib/bundled-plugins
    networks:
      - sonarqube-net
networks:
  sonarqube-net:
volumes:
  sonar_conf:
  sonar_data:
  sonar_extensions:
  sonar_plugins:
  postgres_data:
```

Then I executed the docker-compose. 
`docker-compose up`

Then wait to sonarqube to setup and access from my browser with localhost:9000

![sonar-up](sonar-up.PNG)

Scanner

![sonar-scanner](scanner.png)
